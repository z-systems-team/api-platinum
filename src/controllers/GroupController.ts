import { Request, Response } from 'express';

import CreateGroupService from '../services/CreateGroupService';

// 1
export default class GroupController {
  public async create(request: Request, response: Response): Promise<Response> {
    const { name } = request.body;

    const createGroup = new CreateGroupService(); // 1

    const group = await createGroup.execute({ name });

    return response.status(200).json(group);
  }
}
