import { Request, Response } from 'express';

import CreateUserService from '../services/CreateUserService';
import ListUserService from '../services/ListUserService';

// 1
export default class UsersController {
  public async index(request: Request, response: Response): Promise<Response> {
    const listUsers = new ListUserService();

    const users = await listUsers.execute();

    return response.status(200).json(users);
  }

  public async create(request: Request, response: Response): Promise<Response> {
    const { name, email, password, group_id } = request.body;

    const createUser = new CreateUserService(); // 1

    const user = await createUser.execute({ name, email, password, group_id });

    delete user.password;

    return response.json(user);
  }
}
