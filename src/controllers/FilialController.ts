import { Request, Response } from 'express';

import CreateFilialService from '../services/CreateFilialService';
import ListFilialService from '../services/ListFilialService';

// 1
export default class UsersController {
  public async index(request: Request, response: Response): Promise<Response> {
    const listFiliais = new ListFilialService();

    const filiais = await listFiliais.execute();

    return response.status(200).json(filiais);
  }

  public async create(request: Request, response: Response): Promise<Response> {
    const {
      email,
      cpf_cnpj,
      name,
      name_fantasy,
      cellphone,
      address,
      owner,
      group_id,
      activity_id,
    } = request.body;

    const createFilial = new CreateFilialService(); // 1

    const filial = await createFilial.execute({
      email,
      cpf_cnpj,
      name,
      name_fantasy,
      cellphone,
      address,
      owner,
      group_id,
      activity_id,
    });

    return response.json(filial);
  }
}
