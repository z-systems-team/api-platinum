import { Request, Response } from 'express';

import CreateActivityService from '../services/CreateActivityService';
import ListActivityService from '../services/ListActivityService';

// 1
export default class GroupController {
  public async index(request: Request, response: Response): Promise<Response> {
    const listActivities = new ListActivityService();

    const activities = await listActivities.execute();

    return response.status(200).json(activities);
  }

  public async create(request: Request, response: Response): Promise<Response> {
    const { title, description, group_id } = request.body;

    const createActivity = new CreateActivityService(); // 1

    const activity = await createActivity.execute({
      title,
      description,
      group_id,
    });

    return response.status(200).json(activity);
  }
}
