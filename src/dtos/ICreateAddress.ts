export interface IAddressDataType {
  city: string;
  state: string;
  uf: string;
  neighborhood: string;
  number: string;
  code: string;
  complement: string;
}
