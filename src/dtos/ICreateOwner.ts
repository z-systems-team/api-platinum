import { IAddressDataType } from './ICreateAddress';

export interface IOwnerDataType {
  name: string;
  lastName: string;
  email: string;
  cpf: string;
  cellphone: string;
  birthday: string;
  address: IAddressDataType;
}
