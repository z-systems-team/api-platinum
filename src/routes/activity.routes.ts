import { Router } from 'express';

import ActivityController from '../controllers/ActivityController';
import ensureAuthenticated from '../middlewares/ensureAuthenticathed';
import activityStore from '../middlewares/validators/ActivityStore';

const activityController = new ActivityController();

const activityRouter = Router();

activityRouter.use(ensureAuthenticated);

activityRouter.get('/', activityController.index);
activityRouter.post('/', activityStore, activityController.create);

export default activityRouter;
