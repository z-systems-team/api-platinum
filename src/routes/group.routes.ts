import { Router } from 'express';

import GroupController from '../controllers/GroupController';

const groupController = new GroupController();

const groupRouter = Router();

groupRouter.post('/', groupController.create);

export default groupRouter;
