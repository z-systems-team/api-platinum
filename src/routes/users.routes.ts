import { Router } from 'express';

import UsersController from '../controllers/UsersController';

const usersController = new UsersController();

const usersRoute = Router();

usersRoute.post('/', usersController.create);
usersRoute.get('/', usersController.index);

export default usersRoute;
