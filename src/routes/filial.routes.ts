import { Router } from 'express';

import FilialController from '../controllers/FilialController';
import ensureAuthenticated from '../middlewares/ensureAuthenticathed';

const filialController = new FilialController();

const filialRoute = Router();

filialRoute.use(ensureAuthenticated);

filialRoute.post('/', filialController.create);
filialRoute.get('/', filialController.index);

export default filialRoute;
