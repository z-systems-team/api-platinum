import { Router } from 'express';

import usersRouter from './users.routes';
import sessionRouter from './sessions.routes';
import groupRouter from './group.routes';
import activityRouter from './activity.routes';
import filialRouter from './filial.routes';

const routes = Router();

routes.use('/users', usersRouter);
routes.use('/sessions', sessionRouter);
routes.use('/group', groupRouter);
routes.use('/activity', activityRouter);
routes.use('/filial', filialRouter);

export default routes;
