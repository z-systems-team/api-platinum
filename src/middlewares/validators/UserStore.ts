/* eslint-disable prettier/prettier */
import { Request, Response, NextFunction } from 'express';
import * as Yup from 'yup';

import AppError from '../../errors/AppError';

// 3
export default async (request: Request, response: Response, next: NextFunction) => {
  try { // 1
    const schema = Yup.object().shape({
      title: Yup.string().required(),
      description: Yup.string().max(400).required(),
      group_id: Yup.number().required(),
    });

    await schema.validate(request.body, {
      abortEarly: false,
    });

    return next();
  } catch (error) { // 1
    throw new AppError(error.inner, 400); // 1
  }
}
