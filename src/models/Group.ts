import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('groups')
class Group {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  name: string;
}

export default Group;
