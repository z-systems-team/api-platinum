import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  JoinColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

import { IAddressDataType } from '../dtos/ICreateAddress';
import { IOwnerDataType } from '../dtos/ICreateOwner';

import Activity from './Activity';
import Group from './Group';

@Entity('filial')
class Filial {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  name: string;

  @Column()
  name_fantasy: string;

  @Column()
  email: string;

  @Column()
  cellphone: string;

  @Column()
  cpf_cnpj: string;

  @Column('simple-json')
  address: IAddressDataType;

  @Column('simple-json')
  owner: IOwnerDataType;

  @Column()
  group_id: number;

  @ManyToOne(() => Group, group => group.name)
  @JoinColumn({ name: 'group_id' })
  group: Group;

  @Column()
  activity_id: number;

  @ManyToOne(() => Activity)
  @JoinColumn({ name: 'activity_id' })
  activity: Activity;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;
}

export default Filial;
