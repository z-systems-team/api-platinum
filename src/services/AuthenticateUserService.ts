import { getRepository } from 'typeorm';
import { compare } from 'bcryptjs';
import { sign } from 'jsonwebtoken';
import authConfig from '../config/auth';

import User from '../models/Users';
import AppError from '../errors/AppError';

interface IRequest {
  email: string;
  password: string;
}

interface IResponse {
  user: User;
  token: string;
}

// 4
export default class AuthenticateUserService {
  public async execute({ email, password }: IRequest): Promise<IResponse> {
    const usersRepository = getRepository(User); // 1

    const user = await usersRepository.findOne({ where: { email } });

    if (!user) {
      // 1
      throw new AppError('Incorrect email/password combination'); // 1
    }

    const passwordMatched = await compare(password, user.password);

    if (!passwordMatched) {
      // 1
      throw new AppError('Incorrect email/password combination');
    }

    const { secret, expiresIn } = authConfig.jwt;

    const token = sign({}, secret, {
      subject: String(user.id),
      expiresIn,
    });

    delete user.password;

    return {
      user,
      token,
    };
  }
}
