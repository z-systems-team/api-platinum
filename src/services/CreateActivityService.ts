import { getRepository } from 'typeorm';

import AppError from '../errors/AppError';
import Activity from '../models/Activity';

interface IRequest {
  title: string;
  description: string;
  group_id: number;
}

// 3
export default class CreateActivityService {
  public async execute({
    title,
    description,
    group_id,
  }: IRequest): Promise<Activity> {
    const activityRepository = getRepository(Activity); // 1

    const activityExists = await activityRepository.findOne({
      where: { title },
    });

    if (activityExists) {
      // 1
      throw new AppError('Activity already exists.', 400); // 1
    }

    const activity = activityRepository.create({
      title,
      description,
      group_id,
    });

    await activityRepository.save(activity);

    return activity;
  }
}
