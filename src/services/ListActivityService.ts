import { getRepository } from 'typeorm';

import Activity from '../models/Activity';

export default class ListActivityService {
  public async execute(): Promise<Activity[]> {
    const activitiesRepository = getRepository(Activity);

    const activities = await activitiesRepository.find({
      relations: ['group'],
    });

    return activities;
  }
}
