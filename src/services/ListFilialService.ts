import { getRepository } from 'typeorm';

import Filial from '../models/Filial';

export default class ListFilialService {
  public async execute(): Promise<Filial[]> {
    const filialRepository = getRepository(Filial);

    const filial = await filialRepository.find({
      relations: ['group', 'activity'],
    });

    return filial;
  }
}
