import { getRepository } from 'typeorm';

import { IAddressDataType } from '../dtos/ICreateAddress';
import { IOwnerDataType } from '../dtos/ICreateOwner';

import AppError from '../errors/AppError';
import Filial from '../models/Filial';

interface IRequest {
  name: string;
  name_fantasy: string;
  email: string;
  cellphone: string;
  cpf_cnpj: string;
  address: IAddressDataType;
  owner: IOwnerDataType;
  group_id: number;
  activity_id: number;
}

// 3
export default class CreateFilialService {
  public async execute({
    email,
    cpf_cnpj,
    name,
    name_fantasy,
    cellphone,
    address,
    owner,
    group_id,
    activity_id,
  }: IRequest): Promise<Filial> {
    const filialRepository = getRepository(Filial); // 1

    const checkFilial = await filialRepository.findOne({
      where: [{ email }, { cpf_cnpj }],
    });

    if (checkFilial) {
      // 1
      throw new AppError('Filial Already exists.', 400); // 1
    }

    const filial = filialRepository.create({
      email,
      cpf_cnpj,
      name,
      name_fantasy,
      cellphone,
      address,
      owner,
      group_id,
      activity_id,
    });

    await filialRepository.save(filial);

    return filial;
  }
}
