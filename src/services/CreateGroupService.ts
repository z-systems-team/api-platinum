import { getRepository } from 'typeorm';

import AppError from '../errors/AppError';
import Group from '../models/Group';

interface IRequest {
  name: string;
}

// 3
export default class CreateUserService {
  public async execute({ name }: IRequest): Promise<Group> {
    const groupRepository = getRepository(Group); // 1

    const groupExist = await groupRepository.findOne({ where: { name } });

    if (groupExist) {
      // 1
      throw new AppError('Group already exists.', 400); // 1
    }

    const group = groupRepository.create({
      name,
    });

    await groupRepository.save(group);

    return group;
  }
}
