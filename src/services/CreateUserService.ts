import { getRepository } from 'typeorm';
import { hash } from 'bcryptjs';

import User from '../models/Users';
import AppError from '../errors/AppError';

interface IRequest {
  email: string;
  name: string;
  group_id: number;
  password: string;
}

// 3
export default class CreateUserService {
  public async execute({
    email,
    name,
    password,
    group_id,
  }: IRequest): Promise<User> {
    const usersRepository = getRepository(User); // 1

    const checkUserEmail = await usersRepository.findOne({ where: { email } });

    if (checkUserEmail) {
      // 1
      throw new AppError('Email already in use.', 400); // 1
    }

    const hashedPassword = await hash(password, 8);

    const user = usersRepository.create({
      name,
      email,
      group_id,
      password: hashedPassword,
    });

    await usersRepository.save(user);

    return user;
  }
}
