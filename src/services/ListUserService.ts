import { getRepository } from 'typeorm';

import User from '../models/Users';

export default class ListUserService {
  public async execute(): Promise<User[]> {
    const usersRepository = getRepository(User);

    const users = await usersRepository.find({
      relations: ['group'],
    });

    return users;
  }
}
