import request from 'supertest';
import { verify } from 'jsonwebtoken';
import connection from '../../database/connection';

import authConfig from '../../config/auth';

import app from '../../app';

describe('App', () => {
  beforeAll(async () => {
    await connection.create();
  });

  afterAll(async () => {
    await connection.close();
  });

  beforeEach(async () => {
    await connection.clear();
  });

  it('should be able to create a user', async () => {
    const response = await request(app).post('/users').send({
      email: 'jonhdoe@example.com',
      name: 'john doe',
      password: 'password',
    });

    expect(response.body).toEqual(
      expect.objectContaining({
        email: 'jonhdoe@example.com',
        name: 'john doe',
      }),
    );

    expect(response.status).toBe(200);
  });

  it('should be able to create a user with a group_id', async () => {
    const group = await request(app).post('/group').send({
      name: 'Administrador',
    });

    const response = await request(app).post('/users').send({
      email: 'jonhdoe@example.com',
      name: 'john doe',
      group_id: group.body.id,
      password: 'password',
    });

    expect(response.body).toEqual(
      expect.objectContaining({
        email: 'jonhdoe@example.com',
        name: 'john doe',
        group_id: group.body.id,
      }),
    );

    expect(response.status).toBe(200);
  });

  it('should NOT be able to create a user with an email already in use', async () => {
    const user = await request(app).post('/users').send({
      email: 'jonhdoe@example.com',
      name: 'john doe',
      password: 'password',
    });

    expect(user.body).toEqual(
      expect.objectContaining({
        email: 'jonhdoe@example.com',
        name: 'john doe',
      }),
    );

    const response = await request(app).post('/users').send({
      email: 'jonhdoe@example.com',
      name: 'john doe',
      password: 'password',
    });

    expect(response.status).toBe(400);
  });

  it('should be able to list all users', async () => {
    await request(app).get('/users').expect(200);
  });

  it('should be able to create a new session and receive a token JWT', async () => {
    await request(app).post('/users').send({
      email: 'jonhdoe@example.com',
      name: 'john doe',
      password: 'password',
    });

    const response = await request(app).post('/sessions').send({
      email: 'jonhdoe@example.com',
      password: 'password',
    });

    const tokenVerified = verify(response.body.token, authConfig.jwt.secret);
    expect(tokenVerified).toBeTruthy();
  });

  it('should not be able to create a new session with invalid data', async () => {
    const response = await request(app).post('/sessions').send({
      email: 'jonhdoe@example.com',
      password: 'password',
    });

    expect(response.status).toBe(400);
    expect(response.body).toMatchObject(
      expect.objectContaining({
        status: 'error',
        message: 'Incorrect email/password combination',
      }),
    );
  });

  it('should be able to create a group', async () => {
    const response = await request(app).post('/group').send({
      name: 'Administrador',
    });

    expect(response.body).toEqual(
      expect.objectContaining({
        name: 'Administrador',
      }),
    );
    expect(response.status).toBe(200);
  });

  it('should NOT be able to create a group with same name', async () => {
    const group = await request(app).post('/group').send({
      name: 'Administrador',
    });

    expect(group.body).toEqual(
      expect.objectContaining({
        name: 'Administrador',
      }),
    );

    const response = await request(app).post('/group').send({
      name: 'Administrador',
    });

    expect(response.status).toBe(400);
  });
});
