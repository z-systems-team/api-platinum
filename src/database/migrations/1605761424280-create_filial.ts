import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class createFilial1605761424280 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'filial',
        columns: [
          {
            name: 'id',
            type: 'integer',
            unsigned: true,
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'group_id',
            type: 'integer',
            isNullable: true,
          },
          {
            name: 'activity_id',
            type: 'integer',
            isNullable: true,
          },
          {
            name: 'name',
            type: 'varchar',
          },
          {
            name: 'name_fantasy',
            type: 'varchar',
          },
          {
            name: 'email',
            isUnique: true,
            type: 'varchar',
          },
          {
            name: 'cpf_cnpj',
            isUnique: true,
            type: 'varchar',
          },
          {
            name: 'cellphone',
            type: 'varchar',
          },
          {
            name: 'owner',
            type: 'object',
          },
          {
            name: 'address',
            type: 'object',
          },
          {
            name: 'created_at',
            type: 'timestamp',
            default: 'now()',
          },
          {
            name: 'updated_at',
            type: 'timestamp',
            default: 'now()',
          },
        ],
        foreignKeys: [
          {
            name: 'groupId',
            referencedTableName: 'groups',
            referencedColumnNames: ['id'],
            columnNames: ['group_id'],
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
          },
          {
            name: 'activityId',
            referencedTableName: 'activity',
            referencedColumnNames: ['id'],
            columnNames: ['activity_id'],
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
          },
        ],
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('filial');
  }
}
